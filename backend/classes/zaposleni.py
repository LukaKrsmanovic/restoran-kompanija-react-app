from db import select, insert, update
from db import delete_row as delete
from flask_restful import Resource, reqparse

"""
in args_type
  * 0 represents integer type
  * "" empty string represents string
"""

class Zaposleni(Resource):    
    def get(self):
        query = "select * from Zaposleni"
        _, zaposleni = select(query)
        return zaposleni

    def post(self):
        parser = reqparse.RequestParser()
        parser.add_argument("kompanija_id", required = True, type = int)
        parser.add_argument("ime", required = True, type = str)
        parser.add_argument("prezime", required = True, type = str)
        parser.add_argument("username", required = True, type = str)
        parser.add_argument("sifra", required = True, type = str)
        args = parser.parse_args()
        query = f"""insert into Zaposleni(kompanija_id, ime, username, sifra) 
            values ('{args['kompanija_id']}','{args['ime']}',
            '{args['username']}','{args['sifra']}')"""
        insert(query)

    def put(self):
        parser = reqparse.RequestParser()
        parser.add_argument("id", required = True, type = int)
        parser.add_argument("kompanija_id", required = True, type = int)
        parser.add_argument("ime", required = True, type = str)
        parser.add_argument("prezime", required = True, type = str)
        parser.add_argument("username", required = True, type = str)
        parser.add_argument("sifra", required = True, type = str)
        args = parser.parse_args()
        update(f"""update Zaposleni set kompanija_id = {args['kompanija_id']}, 
                ime = '{args['ime']}', username = '{args['username']}', 
                sifra = '{args['sifra']}' where id = {args['id']}""")

    def delete(self):
        parser = reqparse.RequestParser()
        parser.add_argument("id", required = True, type = int)
        args = parser.parse_args()
        query = f"delete from Zaposleni where id = {args['id']}"
        delete(query)