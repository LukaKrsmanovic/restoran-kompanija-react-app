import React from "react";

function getFoodList(jela) {
  return jela.map((jelo) => {
    let naziv = jelo["jelo_naziv"];
    if(naziv.length > 25) {
      naziv = naziv.substring(0, 22);
      naziv += "...";
    }
    
    return (
      <li>
        {naziv}
        <div className="right floated">
          {jelo["jelo_cijena"].toFixed(2)}
          <i className="small euro sign icon"></i>
        </div>
      </li>
    );
  });
}

function getTotal(jela) {
  let sum = 0;
  jela.map((jelo) => (sum += jelo["jelo_cijena"]));
  return sum.toFixed(2);
}

const Order = (props) => {
  const {restoran_naziv, datum, jela} = props.orderInfo;

  return (
    // Do this with link not a href
    <a
      className="ui card"
      href="http://www.dog.com"
      target="_blank"
      rel="noreferrer"
    >
      <div className="content">
        <div className="header">{restoran_naziv}</div>
        <div className="meta">{datum}</div>
        <div className="description">
          <ul>{getFoodList(jela)}</ul>
        </div>
      </div>
      <div className="extra content">
        <span className="left floated">
          <h3>Total</h3>
        </span>
        <span className="right floated">
          <h3>
            {getTotal(jela)}
            <i className="small euro sign icon"></i>
          </h3>
        </span>
      </div>
    </a>
  );
};

export default Order;
