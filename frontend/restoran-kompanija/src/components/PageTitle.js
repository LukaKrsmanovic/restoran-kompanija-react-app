import React from "react";

const PageTitle = (props) => {
  const { title, icon } = props.pageTitle;
  let classes = "circular icon " + icon;

  return (
    <h2 className="ui center aligned icon header">
      <i className={classes}></i>
      <span className="ui large header restorani-list-title">{title}</span>
    </h2>
  );
};

export default PageTitle;
