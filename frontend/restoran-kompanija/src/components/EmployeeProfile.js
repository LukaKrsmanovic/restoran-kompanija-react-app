import React from "react";
import { useState, useEffect } from "react";
import "../App.css";
import PageTitle from "./PageTitle";
import Loading from "./loading";

const EmployeeProfile = () => {
  let [employee, setEmployee] = useState({
      username: "",
      is_fetched: false,
  });

//   useEffect(() => {
//     // will execute only first time
//     const url = "https://jsonblob.com/api/945488125745184768";
//     fetch(url)
//       .then((resp) => resp.json())
//       .then((data) => {
//         is_fetched = true;
//         setEmployee({
//             ...data
//         });
//       });
//   }, []);

    useEffect(() => {
        setEmployee({
            id: 1,
            kompanija_id: 1,
            ime: "Luka",
            prezime: "Krsmanovic",
            username: "lukak",
            sifra: "luka123",
            novo_ime: "Luka",
            novo_prezime: "Krsmanovic",
            nova_sifra: "",
            nova_sifra_ponovi: "",
            is_fetched: true,
        });
    }, []);

    const inputChangeHandler = (event) => {
        let name = event.target.name;
        let value = event.target.value;
        setEmployee({
            ...employee,
            [name]: value,
        });
    }

    const setNewName = () => {
        setEmployee({
            ...employee,
            ime: employee.novo_ime,
            prezime: employee.novo_prezime,
        });
    }

    const setNewPassword = () => {
        setEmployee({
            ...employee,
            sifra: employee.nova_sifra,
        });
    }

  const displayData = () => {
    const button_classes = "fluid ui button";

    let ime_prez_btn = button_classes;
    if((employee.ime === employee.novo_ime && employee.prezime === employee.novo_prezime) ||
        employee.novo_ime.length < 3 || employee.novo_prezime.length < 3) {
        ime_prez_btn += " disabled";
    }

    let sifra_btn = button_classes;
    if(employee.nova_sifra !== employee.nova_sifra_ponovi ||
        employee.nova_sifra === "") {
        sifra_btn += " disabled";
    }

    return (<div class="ui form profile-form">
    <h2 class="ui dividing header">
        <i class="settings icon"></i>
        <div class="content">
            Promjena Podataka
            <div class="sub header">Možete pormijeniti svoje ime i prezime</div>
        </div>
    </h2>
    <div class="three fields">
      <div class="field">
        <label>Ime</label>
        <input type="text" name="novo_ime" onChange={inputChangeHandler} value={employee.novo_ime} />
      </div>
      <div class="field">
        <label>Prezime</label>
        <input type="text" name="novo_prezime" onChange={inputChangeHandler} value={employee.novo_prezime} />
      </div>
      <div class="field submit-button">
        <button class={ime_prez_btn} onClick={setNewName}>Sačuvaj izmjene</button>
      </div>
    </div>
    <br /><br />
    <h2 class="ui dividing header">
        <i class="lock icon"></i>
        <div class="content">
            Šifra Profila
            <div class="sub header">Možete promijeniti svoju šifru</div>
        </div>
    </h2>
    <div class="three fields">
      <div class="field">
        <label>Trenutna šifra</label>
        <input 
            type="password" 
            placeholder="Current password"
            name="nova_sifra" 
            onChange={inputChangeHandler} 
            value={employee.nova_sifra}  />
      </div>
      <div class="field">
        <label>Nova šifra</label>
        <input 
            type="password" 
            placeholder="New password"
            name="nova_sifra_ponovi" 
            onChange={inputChangeHandler} 
            value={employee.nova_sifra_ponovi}  />
      </div>
      <div class="field submit-button">
        <button class={sifra_btn} onClick={setNewPassword}>Sačuvaj izmjene</button>
      </div>
    </div>
  </div>);
  };

  let username = "lukak";
  let profil_title = employee.is_fetched ? employee.username : "Profil";

  return (
    <div className="">
      <PageTitle pageTitle={{ title: profil_title, icon: "user" }} />
      <br />

      {Object.keys(employee).length !== 0 ? (
        displayData()
      ) : (
        <Loading subject="profil" fetched={employee.is_fetched} />
      )}
    </div>
  );
};

export default EmployeeProfile;
