import React, { useEffect, useState } from "react";
import "../App.css";

const FoodCard = (props) => {
  let { id, restoran_id, naziv, opis, cijena, slika, quantity } = props.foodInfo;

  // if opis too long than put ...
  if (opis.length > 100) {
    opis = opis.substring(0, 97);
    opis += "...";
  }

  return (
    <div className="card">
      <div className="content">
        <div
          id="food-image"
          className="right floated ui mini"
          style={{
            backgroundImage: `url(${slika})`,
          }}
        ></div>
        <div className="header">{naziv}</div>
        <div className="meta">
          {cijena.toFixed(2)}
          <i className="euro sign icon"></i>
        </div>
        <div className="description">{opis}</div>
      </div>
      <div className="extra content">
        <div className="ui right aligned quantity">
          Količina: <span className="">{quantity}</span>
        </div>
        <div className="ui two buttons">
          <div
            className="ui basic red button"
            onClick={() =>
                  props.totalChange(id, -1, parseFloat(props.total - cijena))
            }
          >
            Ukloni
          </div>
          <div
            className="ui basic green button"
            onClick={() =>
                props.totalChange(id, 1, parseFloat(props.total + cijena))
            }
          >
            Dodaj
          </div>
        </div>
      </div>
    </div>
  );
};

export default FoodCard;
