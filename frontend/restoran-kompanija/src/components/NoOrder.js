import React from "react";

const NoOrder = (props) => {
  return (
    <div className="ui icon no-order message">
      <img
        src="https://img.freepik.com/free-vector/flying-slice-pizza-cartoon-vector-illustration-fast-food-concept-isolated-vector-flat-cartoon-style_138676-1934.jpg?size=338&ext=jpg&ga=GA1.2.1222885761.1641945600"
        alt="food"
        width="100px"
        height="100px"
      />
      <div className="content">
        <div className="header">Poruči nešto sada</div>
        <p>Nema {props.section} porudžbina.</p>
      </div>
    </div>
  );
};

export default NoOrder;
