import React, { useEffect, useState } from "react";
import "../App.css";
import PageTitle from "./PageTitle";
import NoOrder from "./NoOrder";
import CurrentOrder from "./CurrentOrder";
import OrdersContainer from "./OrdersContainer";
import Loading from "./loading";

const url = "https://jsonblob.com/api/945987540457111552";

const OrdersOverview = (props) => {
  let [orders, setOrders] = useState({
    current: [],
    past: [],
    fetched: false,
  });

  useEffect(() => {
    fetch(url)
    .then((resp) => resp.json())
    .then((data) => {
      let orders_data = orders;

      orders_data.current = data.filter(
        order => order.status !== "delivered"
      );

      orders_data.past = data.filter(
        order => order.status === "delivered"
      );

      setOrders({
        ...orders_data,
        fetched: true,
      });
    });
  }, []);

  return (
    <div className="orders-page">
      <PageTitle
        pageTitle={{ title: "Porudžbine", icon: "list alternate outline" }}
      />
      <h2 className="ui left aligned">
        <i className="hourglass half icon"></i>Trenutna porudžbina
      </h2>
      {orders.current.length === 0 ? 
        <Loading subject="porudzbina" section="trenutnih" fetched={orders.fetched} /> 
        : <CurrentOrder orders_list={orders.current} />}
      <br />
      <h2>
        <i className="clock outline icon"></i>Prethodne porudžbine
      </h2>
      {orders.past.length === 0 ? 
        <Loading subject="porudzbina" section="prethodnih" fetched={orders.fetched} /> 
        : <OrdersContainer orders_list={orders.past} />}
      <br />
      <br />
    </div>
  );
};

export default OrdersOverview;
