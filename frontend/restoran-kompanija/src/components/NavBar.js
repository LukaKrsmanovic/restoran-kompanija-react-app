import React from "react";
import { Link } from "react-router-dom";
import "../App.css";

const NavBar = () => {
  return (
    <div className="navbar">
      <div className="container ui secondary menu">
        <Link to={{ pathname: "/" }}>
          <h1 className="header item">Klovo</h1>
        </Link>
        <div className="right menu">
          <Link to={{ pathname: "/restaurants-list" }} className="ui item">
            Restorani
          </Link>
          <Link to={{ pathname: "/orders" }} className="ui item">
            Porudžbine
          </Link>
          <Link to={{ pathname: "/profile" }} className="ui item">
            Profil
          </Link>
          <Link to={{ pathname: "/company-profile" }} className="ui item">
            Kompanija
          </Link>
          <a className="ui item">Logout</a>
        </div>
      </div>
    </div>
  );
};

export default NavBar;
