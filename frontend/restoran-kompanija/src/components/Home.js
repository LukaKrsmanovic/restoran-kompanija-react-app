import React from "react";
import { useState, useEffect } from "react";
import "../App.css";
import Loading from "./loading";
import { Link } from "react-router-dom";
import SimpleImageSlider from "react-simple-image-slider";

const Home = () => {
    const [images, setImages] = useState([
        {
            url: "https://media.istockphoto.com/vectors/cafe-vector-id1175445101?k=20&m=1175445101&s=612x612&w=0&h=2XcBDfFa6BBDIe-G1nqMDg9v2EWy5Yaj-byUncLcjZA="
        },
        {
            url: "https://media.istockphoto.com/vectors/cafe-vector-id1175445101?k=20&m=1175445101&s=612x612&w=0&h=2XcBDfFa6BBDIe-G1nqMDg9v2EWy5Yaj-byUncLcjZA="
        },
    ]);

    useEffect(() => {
        // Load once when page opens
        const url = "https://jsonblob.com/api/945488125745184768"
        fetch(url)
            .then((resp) => resp.json())
            .then((data) => {
                const imgs = data.map((restaurant) => {
                    return {url: restaurant.slika};
                })
                setImages([...imgs])
            }
        );
    }, []);

    const slider_width = window.innerWidth < 800 ? 350 : 800;
    const slider_height = window.innerWidth < 800 ? 300 : 400;

  return (
        <div className="home">
            <section>
                <div className="ui container">
                    <h1 className="ui header">Welcome To Klovo</h1>  
                    <h3>
                        Lorem ipsum dolor sit amet consectetur adipisicing elit. Facilis nam, nemo necessitatibus quod fugiat repellendus ratione vel, laudantium esse et ipsum corporis porro vitae odit asperiores quibusdam eligendi deserunt corrupti? Lorem ipsum, dolor sit amet consectetur adipisicing elit. Laudantium rem in cumque commodi facilis tempora repudiandae neque est iste esse, nostrum aliquid eligendi, sint voluptate unde eum? Lorem ipsum, dolor sit amet consectetur adipisicing elit. Laudantium, rem aut!
                    </h3>
                </div>
            </section>
            <section>
                <div className="ui container">
                    <h1 className="ui header">Restorani U Ponudi</h1>
                    <div className="image-container">
                        <SimpleImageSlider
                            width={slider_width}
                            height={slider_height}
                            images={images}
                            showBullets={true}
                            showNavs={false}
                            autoPlay={true}
                        />  
                    </div>     
                </div>
            </section>
            <section>
                <div className="ui container">
                    <h1 className="ui header">Prijavi Se Kao</h1>  
                    <div className="ui three stackable link cards">
                        <Link
                            to={{
                                pathname: "/restaurants-list",
                            }}
                            className="ui card"
                            >
                            <div
                                className="image"
                                style={{ backgroundImage: `url("https://cdn2.iconfinder.com/data/icons/scenarium-vol-4/128/018_avatar_support_girl_woman_female_profile_account-512.png")`, backgroundSize: "100% 100%" }}
                            ></div>
                            <div className="content">
                                <div className="header">Zaposleni</div>
                            </div>
                        </Link>
                        <Link
                            to={{
                                pathname: "/restaurants-list",
                            }}
                            className="ui card"
                            >
                            <div
                                className="image"
                                style={{ backgroundImage: `url("https://cdn0.iconfinder.com/data/icons/scenarium-vol-14/128/047_fast_food_restaurant_burger-512.png")`, backgroundSize: "100% 100%" }}
                            ></div>
                            <div className="content">
                                <div className="header">Restoran</div>
                            </div>
                        </Link>
                        <Link
                            to={{
                                pathname: "/restaurants-list",
                            }}
                            className="ui card"
                            >
                            <div
                                className="image"
                                style={{ backgroundImage: `url("https://cdn2.iconfinder.com/data/icons/scenarium-vol-4/128/031_brainstorm_meeting_crowd_people_group_communicatation_team-512.png")`, backgroundSize: "100% 100%" }}
                            ></div>
                            <div className="content">
                                <div className="header">Kompanija</div>
                            </div>
                        </Link>
                    </div>
                </div>
            </section>
        </div>
  );
};

export default Home;
