import React from "react";
import { useState, useEffect } from "react";
import "../App.css";
import PageTitle from "./PageTitle";
import Loading from "./loading";
import FoodCard from "./FoodCard";

let url = "";

const RestaurantFoodList = (props) => {
  const restaurant = props.history.location.restaurantInfo;

  // food_cart is object that has food list and 
  // total amount of selected food for user
  let [food_cart, setFoodCart] = useState({
    food: [],
    total: 0,
    fetched: false,
  });

  // Child - parent komunikacija
  const handleTotalChange = (id, action, total) => {
    const food_index = food_cart.food.findIndex((item) => item.id === id);
    let new_food_cart = food_cart;
    let quantity = new_food_cart.food[food_index].quantity;

    quantity = action === 1 ? quantity + 1 : quantity - 1;

    // handle variables if they fall below 0
    quantity = quantity < 0 ? 0 : quantity;
    total = total < 0 ? 0 : total;

    new_food_cart.food[food_index].quantity = quantity;
    new_food_cart.total = parseFloat(total.toFixed(2));

    setFoodCart({
      ...new_food_cart
    });
  };

  useEffect(() => {
    // will execute only first time
    url = "";
    if (restaurant.id === 1) {
      url = "https://jsonblob.com/api/945653056968867840";
    }
    fetch(url)
        .then((resp) => resp.json())
        .then((data) => {
          let new_food_cart = food_cart;
          // add quantity property to every food object
          new_food_cart.food = data.map((item) => {
            item["quantity"] = 0;
            return item;
          });
          setFoodCart({
            ...new_food_cart,
            fetched: true,
          });
        })
        .catch((err) => setFoodCart({
          ...food_cart,
          fetched: true,
        }));
  }, []);

  useEffect(() => {
  }, [food_cart]);

  const displayData = () => {
    return (
      <div className="ui three doubling cards left aligned container restaurant-food-list">
        {food_cart.food.map((item) => {
          return (
            <FoodCard
              total={food_cart.total}
              totalChange={handleTotalChange}
              foodInfo={item}
              key={item.id}
            />
          );
        })}
      </div>
    );
  };

  const storeData = () => {};

  const show_button = "button-container ui container";
  const hide_button = "button-container ui container hide";

  return (
    <div className="restaurant-food-list">
      <PageTitle
        pageTitle={{ title: restaurant.naziv, icon: "shopping basket" }}
      />

      {food_cart.food.length !== 0 ? displayData() : <Loading subject="Hrane" fetched={food_cart.fetched} />}

      <div className={food_cart.total === 0 ? hide_button : show_button} onClick={() => console.log(food_cart)}>
        <div
          className="ui vertical animated fluid button large text"
          onClick={storeData()}
        >
          <div className="hidden content">
            <i className="shop icon"></i>
          </div>
          <div className="visible content">
            Pošalji u korpu ( {food_cart.total.toFixed(2)}{" "}
            <i className="small euro sign icon"></i>)
          </div>
        </div>
      </div>
    </div>
  );
};

export default RestaurantFoodList;
