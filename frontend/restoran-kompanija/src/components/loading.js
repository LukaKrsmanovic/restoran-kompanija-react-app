import React, { useEffect, useState } from "react";
import "../App.css";
import NoOrder from "./NoOrder";

const restaurantMessage = () => {
  return (<div className="center-item">
    <div className="ui icon message no-restaurant-items">
      <i className="clipboard outline icon"></i>
      <div className="content">
        <div className="header">
          Prazna lista!
        </div>
        <p>Restoran nije dodao listu hrane, odaberite drugi restoran.</p>
      </div>
    </div>
  </div>);
}

const ordersMessage = (section) => {
  return <NoOrder section={section} />
}

const profileMessage = () => {
  return (<div className="center-item">
    <div className="ui icon negative message no-restaurant-items">
      <i className="exclamation circle icon"></i>
      <div className="content">
        <div className="header">
          Greška pri učitavanju!
        </div>
        <p>Nije moguće učitati podatke za ovaj profil.</p>
      </div>
    </div>
  </div>);
}

const Loading = (props) => {
  let [is_loading, setIsLoading] = useState(true);

  let subject = props.subject;

  if (!subject) {
    subject = "";
  }

  useEffect(() => {
    setTimeout(() => setIsLoading(false), 2000);
  }, [])

  let message = restaurantMessage();
  if(props.subject === "porudzbina") {
    message = ordersMessage(props.section);
  } else if(props.subject === "profil") {
    message = profileMessage(); 
  }

  return (
    (is_loading || !props.fetched) ?
    (<div className="ui segment" style={{ padding: "3rem" }}>
      <div className="ui active inverted dimmer">
        <div className="ui text loader">Učitavanje {subject}</div>
      </div>
      <p></p>
    </div>) : message
  );
};

export default Loading;
