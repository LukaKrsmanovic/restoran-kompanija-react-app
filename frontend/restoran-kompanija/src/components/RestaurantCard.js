import React from "react";
import { Link } from "react-router-dom";
import "../App.css";

const RestaurantCard = (props) => {
  const { id, naziv, slika, pocetak, kraj } = props.restaurantInfo;

  let restaurant_state = calcRestaurantState(pocetak, kraj);
  let classes = "extra content " + restaurant_state.toLowerCase();

  let is_open = restaurant_state === "Otvoreno";

  return (
    <Link
      to={{
        pathname: is_open ? "/restaurants-list/restaurant-food-list" : "",
        restaurantInfo: props.restaurantInfo,
      }}
      className="ui card"
    >
      <div
        className="image"
        style={{ backgroundImage: `url(${slika})`, backgroundSize: "cover" }}
      ></div>
      <div className="content">
        <div className="header">{naziv}</div>
        <div className="meta">
          <span className="date">
            <i className="clock outline icon"></i>
            {pocetak} - {kraj}
          </span>
        </div>
      </div>
      <div className={classes}>{restaurant_state}</div>
    </Link>
  );
};

const calcRestaurantState = (pocetak, kraj) => {
  // pocetak, kraj look like this xx:xx
  // where first xx is hours from 00 to 23
  // and second xx is minutes from 00 to 59

  let hours = parseInt(pocetak.substring(0, 2));
  let minutes = parseInt(pocetak.substring(3, 5));
  let start_minutes = hours * 60 + minutes;

  hours = parseInt(kraj.substring(0, 2));
  minutes = parseInt(kraj.substring(3, 5));
  let end_minutes = hours * 60 + minutes;

  let current = new Date();
  let current_minutes = current.getHours() * 60 + current.getMinutes();

  let is_open =
    start_minutes < current_minutes && current_minutes < end_minutes;

  return is_open ? "Otvoreno" : "Zatvoreno";
};

export default RestaurantCard;
