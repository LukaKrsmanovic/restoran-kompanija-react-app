import React from "react";
import Order from "./Order";

function showOrders(orders) {
  if(orders.length === 0) {
    return "";
  }
  return orders.map((order) => {
    return <Order orderInfo={order} />;
  });
}

const OrdersContainer = (props) => {
  const orders = props.orders_list;

  return (
    <div className="ui stackable cards centered message">{showOrders([...orders].reverse())}</div>
  );
}

export default OrdersContainer;