import React from "react";
import { useState, useEffect } from "react";
import "../App.css";
import PageTitle from "./PageTitle";
import Loading from "./loading";

const CompanyProfile = () => {
  let [company, setCompany] = useState({
      naziv: "",
      is_fetched: false,
  });

  const url = "https://jsonblob.com/api/947596487760822272";
  
    useEffect(() => {
        fetch(url)
            .then(resp => resp.json())
            .then(data => {
                setCompany({
                    id: 1,
                    naziv: "Bixbit",
                    sifra: "bixbit123",
                    broj_zap: 5,
                    dnevni_limit: 5,
                    zaposleni: data,
                    novi_naziv: "Bixbit",
                    novi_dn_limit: 5,
                    is_fetched: true,
                });
        });
    }, []);

  const displayData = () => {
      return (<div className="">
            {dailyLimitPart()}
            <br />
            <h2 className="ui dividing header">
                <i className="address book outline icon"></i>
                <div className="content">
                    Lista Zaposlenih
                    <div className="sub header">Pregled podataka zaposlenih radnika</div>
                </div>
            </h2>
            <table className="ui celled table">
                <thead>
                    <tr>
                        <th>Ime</th>
                        <th>Prezime</th>
                        <th>Username</th>
                    </tr>
                </thead>
                <tbody>
                    {company.zaposleni.map(zaposleni => {
                        return (<tr key={zaposleni.id}>
                            <td data-label="fname">{zaposleni.ime}</td>
                            <td data-label="lname">{zaposleni.prezime}</td>
                            <td data-label="username">{zaposleni.username}</td>
                        </tr>);
                    })}
                </tbody>
            </table>
        </div>);
  };

  const inputChangeHandler = (event) => {
      let name = event.target.name;
      let value = event.target.value;

      if(name === "novi_dn_limit") {
          value = parseInt(value);
      }

      console.log(name, value, typeof(value));
      setCompany({
          ...company,
          [name]: value,
      })
  }

  const setNewData = () => {
      setCompany({
          ...company,
          naziv: company.novi_naziv,
          dnevni_limit: company.novi_dn_limit,
      })
  }

  const dailyLimitPart = () => {
      let btn_classes = "fluid ui button";
      if((company.novi_naziv === company.naziv && company.dnevni_limit === company.novi_dn_limit) 
            || company.novi_naziv === "" || company.novi_dn_limit < 0) {
          btn_classes += " disabled";
      }

      return (<div className="ui form profile-form">
        <h2 className="ui dividing header">
            <i className="settings icon"></i>
            <div className="content">
                Promjena Podataka
                <div className="sub header">Možete pormijeniti sljedeće podatke firme</div>
            </div>
        </h2>
        <div className="three fields">
            <div className="field">
                <label>Naziv firme</label>
                <input type="text" name="novi_naziv" onChange={inputChangeHandler} value={company.novi_naziv} />
            </div>
            <div className="field">
                <label>Dnevni limit</label>
                <input type="number" min="1" name="novi_dn_limit" onChange={inputChangeHandler} value={company.novi_dn_limit} />
            </div>
            <div className="field submit-button">
                <button className={btn_classes} onClick={setNewData} >Sačuvaj izmjene</button>
            </div>
        </div>
      </div>);
  }

  let profil_title = company.is_fetched ? company.naziv : "Profil";

  return (
    <div className="">
      <PageTitle pageTitle={{ title: profil_title, icon: "building outline" }} />
      <br />

      {Object.keys(company).length !== 2 ? (
        displayData()
      ) : (
        <Loading subject="profil" fetched={company.is_fetched} />
      )}
    </div>
  );
};

export default CompanyProfile;
