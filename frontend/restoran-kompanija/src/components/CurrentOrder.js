import React from "react";

const img_src =
  "https://img.freepik.com/free-vector/flying-slice-pizza-cartoon-vector-illustration-fast-food-concept-isolated-vector-flat-cartoon-style_138676-1934.jpg?size=338&ext=jpg&ga=GA1.2.1222885761.1641945600";

const CurrentOrder = (props) => {
  const orders = props.orders_list[0];

  return (
    <div className="ui icon message current-order">
      <div className="content">
        <h2 className="ui header">{orders["restoran_naziv"]}</h2>
        <span className={"status " + orders["status"]}>{orders["status"]}</span>
        <br />

        <div className="ui internally celled stackable grid">
          <div className="ten wide column">
            {showOrder(orders["jela"])}
          </div>
          <div className="six wide column">
            <div className="content">
              <h2 className="ui header">Your orders</h2>
              {orderDetails(orders["jela"])}
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

const showOrder = (orders) => {
  if(orders.length === 0) {
    return "";
  }
  return(
    <div className="ui doubling centered cards">
      {orders.map(order => {
        let naziv = order["jelo_naziv"];

        let slika = order["jelo_slika"];
        if(slika === "") {
          slika = img_src;
        }

        let opis = order["jelo_opis"];
        if(opis.length > 24) {
          opis = opis[0] + "...";
        }

        return (<div className="ui card">
        <div className="image">
          <img src={slika} alt="food" />
        </div>
        <div className="content">
          <div className="header">{naziv}</div>
          <div className="description">{opis}</div>
        </div>
      </div>);
      })}
    </div>
  )
}

const orderDetails = (orders) => {
  if(orders.length === 0) {
    return "";
  }

  let total = 0;

  return (
    <div className="ui middle aligned divided list">
      {orders.map(order => {
        let naziv = order["jelo_naziv"];

        let slika = order["jelo_slika"];
        if(slika === "") {
          slika = img_src;
        }

        let cijena = order["jelo_cijena"];
        total += cijena;

        return (
          <div className="item">
            <div className="right floated content">
              {cijena.toFixed(2)}<i className="small euro sign icon"></i>
            </div>
            <img className="ui avatar image" src={slika} alt="food" />
            <div className="content">{naziv}</div>
          </div>
        );
      })}
      <div className="item">
        <h3 className="right floated content">
          {total.toFixed(2)}<i className="small euro sign icon"></i>
        </h3>
        <h3 className="ui header">Total</h3>
      </div>
    </div>
  );
}

export default CurrentOrder;
