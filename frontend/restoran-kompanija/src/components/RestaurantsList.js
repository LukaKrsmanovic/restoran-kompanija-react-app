import React from "react";
import { useState, useEffect } from "react";
import "../App.css";
import PageTitle from "./PageTitle";
import Loading from "./loading";
import RestaurantCard from "./RestaurantCard";

const RestaurantsList = () => {
  let [restaurants, setRestaurants] = useState([]);
  let is_fetched = false;

  useEffect(() => {
    // will execute only first time
    const url = "https://jsonblob.com/api/945488125745184768";
    fetch(url)
      .then((resp) => resp.json())
      .then((data) => {
        is_fetched = true;
        setRestaurants((restaurants) => (restaurants = data));
      });
  }, []);

  const displayData = () => {
    return (
      <div className="ui two doubling link cards">
        {restaurants.map((restaurant) => {
          return (
            <RestaurantCard restaurantInfo={restaurant} key={restaurant.id} />
          );
        })}
      </div>
    );
  };

  return (
    <div className="restaurant-list">
      <PageTitle pageTitle={{ title: "Restorani", icon: "utensils" }} />

      {restaurants.length !== 0 ? (
        displayData()
      ) : (
        <Loading subject="Restorana" fetched={is_fetched} />
      )}
    </div>
  );
};

export default RestaurantsList;
