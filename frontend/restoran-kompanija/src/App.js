import { BrowserRouter, Route } from "react-router-dom";
import "./App.css";
import NavBar from "./components/NavBar";
import Home from "./components/Home";
import RestaurantsList from "./components/RestaurantsList";
import RestaurantFoodList from "./components/RestaurantFoodList";
import OrdersOverview from "./components/OrdersOverview";
import EmployeeProfile from "./components/EmployeeProfile";
import CompanyProfile from "./components/CompanyProfile";

function App() {
  return (
    <div className="App">
      <BrowserRouter>
        <NavBar />
        <Route exact path="/" component={Home}></Route>
        <div className="ui container pages">
          <Route
            exact
            path="/restaurants-list"
            component={RestaurantsList}
          ></Route>
          <Route
            path="/restaurants-list/restaurant-food-list"
            component={RestaurantFoodList}
          ></Route>
          <Route path="/orders" component={OrdersOverview}></Route>
          <Route path="/profile" component={EmployeeProfile}></Route>
          <Route path="/company-profile" component={CompanyProfile}></Route>
        </div>
      </BrowserRouter>
    </div>
  );
}

export default App;
